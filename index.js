const express = require('express')
const port = 5005;
const cors = require('cors');
const app = express();

app.use(cors())
app.use(express.json())

const router = require('./src/route')
app.use('/api/v1', router)

app.listen(port,() => {
  console.log(`api running on port ${port}`);
})

