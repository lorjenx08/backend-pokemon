'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class mypokemon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  mypokemon.init({
    name: DataTypes.STRING,
    photo_default: DataTypes.STRING,
    abilities: DataTypes.STRING,
    photo_front: DataTypes.STRING,
    photo_back: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'mypokemon',
  });
  return mypokemon;
};