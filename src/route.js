const express = require('express')
const router = express.Router()

const {getAllPokemon, detailPokemon,saveMyPokemon, cekMyPokemon, GetMyPokemon} = require('./controllers/poke')

router.get('/pokemons', getAllPokemon)
router.get('/pokemon-detail/:name', detailPokemon)
router.post('/pokemon', saveMyPokemon)
router.get('/my-pokemon/:name', cekMyPokemon)
router.get('/my-pokemons', GetMyPokemon)

module.exports = router