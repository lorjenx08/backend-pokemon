const axios = require('axios')
const {mypokemon} = require('../../models')

exports.getAllPokemon = async (req,res) => {
  try {
    const data = await axios({
      method: 'get',
      url: `https://pokeapi.co/api/v2/pokemon?limit=1154&offset=0`,
    })
    const total = data?.data?.results?.length / 2;
    const fixdata = data.data.results.slice(0, total)

    const offset = parseInt(req.query.offset);
    const limit = parseInt(req.query.limit);
    const start = (offset - 1) * limit;
    const end = offset * limit

    const paginate = fixdata.slice(start, end)
    const datas = []
    for (let index = 0; index < paginate.length; index++) {
      const urlPhoto = await axios.get(`https://pokeapi.co/api/v2/pokemon/${paginate[index].name}`)
      const img = urlPhoto?.data.sprites?.other["official-artwork"].front_default
      const weight = urlPhoto.data.weight
      datas.push({
        name: paginate[index].name,
        photo: img,
        weight,
      })
    }
    res.send({
      status: 'succes',
      data: datas,
      count: total,
    })
  } catch (error) {
    console.log(error);
    return res.status(500).send({
        status: 'error',
        error: error,
      })
  }
}


exports.detailPokemon = async (req,res) => {
  try {
    const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${req.params.name}`)
    const data = {
      name: response?.data?.species?.name,
      photo: response.data.sprites?.other["official-artwork"].front_default,
      abilities: [response.data.abilities[0].ability?.name, response.data.abilities[1].ability?.name],
      weight: response?.data?.weight,
      action: {
        front: response.data.sprites?.front_default,
        back: response.data.sprites?.back_default,
      }
    }
    return res.send({
      status: 'success',
      data,
    })
  } catch (error) {
      res.status(500).send({
        status: 'error',
        message: 'request error'
      })
  }
}

exports.saveMyPokemon = async (req,res) => {
  try {
    const cekName = await mypokemon.findOne({
      where: {
        name: req.body.name
      }
    })
    if(cekName) {
      return res.status(400).send({
        status: 'badrequest',
        message: 'is pokemon Already exist'
      })
    }
    const data = await mypokemon.create({
      name: req.body.name,
      photo_default: req.body.photo_default,
      photo_front: req.body.photo_front,
      photo_back: req.body.photo_back,
      abilities: req.body.abilities,
    })
    res.send({
      status: 'success',
      message: `add pokemon success ${data.name}`
    })
  } catch (error) {
    res.status(500).send({
      status: 'error',
      message: 'request error'
    })
    
  }
}
exports.cekMyPokemon = async (req,res) => {
  try {
    const data = await mypokemon.findOne({
      where: {
        name: req.params.name
      }
    })
    console.log(data);
    if(data) {
      return res.send({
        status: 'badrequest',
        message: 'is pokemon Already exist',
        type: 0,
      })
    }
    return res.send({
      status: 'success',
      message: 'ready',
      type: 1,
    })

  } catch (error) {
    res.status(500).send({
      status: 'error',
      message: 'request error'
    })
  }
}

exports.GetMyPokemon = async(req, res) => {
  try {
    const data = await mypokemon.findAll({
      attributes: {
        exclude: ["updatedAt"],
      },
    })
    res.send({
      status: 'success',
      data
    })
  } catch (error) {
    res.status(500).send({
      status: 'error',
      message: 'request error'
    })
  }
}